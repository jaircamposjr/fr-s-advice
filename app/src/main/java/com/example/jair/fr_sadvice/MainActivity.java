package com.example.jair.fr_sadvice;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        try {
            readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
   public String[] readCSV() throws IOException {
       String csvFile = "data.csv";
       String line = "";
       String csvSplitBy = ",";

       BufferedReader br = new BufferedReader(new FileReader((csvFile)));

       String[] category = new String[0];
       while ((line = br.readLine()) != null) {
           category = line.split(csvSplitBy);
       }
       return category;
   }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        TextView display = (TextView) findViewById(R.id.mainText);
        int id = item.getItemId();
        String[] category = new String[0];
        try {
            category = readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (id == R.id.brakes) {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "cooling") {
                    if (category[i] == "brakes") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.nav_cooling) {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "drivetrain") {
                    if (category[i] == "cooling") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.drivetrain) {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "engine") {
                    if (category[i] == "drivetrain") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.engine) {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "exhaust") {
                    if (category[i] == "engine") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.exhaust) {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "exterior") {
                    if (category[i] == "exhaust") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.exterior) {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "intake") {
                    if (category[i] == "exterior") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.intake)   {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "interior") {
                    if (category[i] == "intake") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.interior) {
            for(int i=0;i<category.length;i++) {
                while (category[i] != "suspension") {
                    if (category[i] == "interior") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.suspension){
            for(int i=0;i<category.length;i++) {
                while (category[i] != "wheels") {
                    if (category[i] == "suspension") {
                        display.setText(category[i]);

                    }
                }
            }
        } else if (id == R.id.wheels){
            for(int i=0;i<category.length;i++) {
                while (category[i] != "") {
                    if (category[i] == "wheels") {
                        display.setText(category[i]);

                    }
                }
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
